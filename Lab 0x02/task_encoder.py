""" @file               task_encoder.py
    @brief
    @details
    @author
    @date
"""

# period = 65535
# prescaler = ???
# class for encoder driver, encoder, UI
import encoder
import utime

encoder = encoder.Encoder()

class Task_Encoder:
    """ @brief                  Interface with quadrature encoders
        @details
    """
    
    def __init__(self, period, state, UI_print):
        """ @brief              Constructs an encoder object
            @details
        """
        self.state = state
        self.UI_print = UI_print
        self.runs = 0
        self.period = period
        self.previous_time = utime.ticks_us()
        self.next_time = self.period
        self.boolean = False
        encoder.update()
        
    def run(self):
        encoder.update()
        self.current_ticks = utime.ticks_us()
        self.current_time = utime.ticks_diff(self.current_ticks, 
                                             self.previous_time) / 1000000
        if (self.current_time - self.next_time >= 0):
            if (self.state.read() == 0):
                # Run State 0
                encoder.get_position()
                # Update next time variables
                self.next_time += self.period
                
        # State 1 Zero Position
        if (self.state.read() == 1):
            self.zero_pos()
            self.state.write(0)
            
        # State 2 Print Posiiton of Encoder
        elif (self.state.read() == 2):
            if (self.UI_print.num_in() == 0):
                self.UI_print.put(encoder.get_position())
                self.state.write(0)
            
        elif (self.state.read() == 3):
            if (self.UI_print.num_in() == 0):
                self.UI_print.put(encoder.get_delta())
                self.state.write(0)
                
        elif (self.state.read() == 4):
            # If you haven't started collecting data, boolean is false
            # thus start
            if not (self.boolean):
                self.startDataCollect()
            self.collectData()
            
            
        elif (self.state.read() == 5):
            self.endDataCollect()

    
    def zero_pos(self):
        encoder.position = 0
            
    def transitionTo(): # Maybe need this
        pass
    
            
    def startDataCollect(self):
        self.boolean = True
        self.start_ticks = utime.ticks_us()
        self.data = []
        
    def collectData(self):
        self.current_ticks = utime.ticks_us()
        self.current_time = utime.ticks_diff(self.current_ticks, 
                                             self.previous_time) / 1000000
        if (utime.ticks_diff(self.current_ticks, self.start_ticks) < 3000000):
            if (self.current_time - self.next_time >= 0):
                print('Hello World')
                self.data.append(tuple([utime.ticks_diff(
                    self.current_ticks, self.start_ticks) / 1000000, 
                    encoder.get_position()]))
                self.next_time += self.period
        else:
            self.endDataCollect()
        
    def endDataCollect(self):
        self.boolean = False
        self.state.write(0)
        self.UI_print.put(self.data)
            
        

        