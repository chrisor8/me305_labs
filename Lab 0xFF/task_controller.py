""" @file               task_controller.py
    @brief              Controller task providing motor duty cycles based on various
                        inputs
    @details            The controller task is responsible for gathering relevant 
                        positional/velocity from IMU and touch panel and using
                        calculated gains to determine the duty cycle for each motor
                        to balance a ball on platform.
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
"""

class Task_Controller:
    """     @brief              Class for a full state feedback controller task
            @details            This class contains one function which calculates
                                the required duty cycle to operate the ball and
                                platform system.
        """

    def __init__(self, x, th, dx, dth, duty, touch):
        """ @brief              Constructs one instance of the controller task
            @details            This constructs an instance of the controller task
                                and also establishes the gains to run the controller.
            @param  x           A share. Share object describing the x/y position
                                of the ball.
            @param  th          A share. Share object describing the tilt angle
                                of the platform.
            @param  dx          A share. Share object describing the x/y velocity
                                of the ball.
            @param  dth         A share. Share object describing the rotational
                                velocity of the platform.
            @param  duty        A share. Share object describing the duty cycle
                                for a motor.
            @param  touch       A share. Share object describing whether an object
                                is touching the panel.
        """
        
        # Shares
        ## @brief This object is a shares object describing the position of the ball
        self.x = x
        ## @brief This object is a shares object describing the tilt angle of the platform
        self.th = th
        ## @brief This object is a shares object describing the velocity of the ball
        self.dx = dx
        ## @brief This object is a shares object describing the rotational velocity of the platform
        self.dth = dth
        ## @brief This object is a shares object describing the duty cycle for a motor
        self.duty = duty
        ## @brief This object is a shares object describing whether or not an object is touching the panel
        self.touchbool = touch
        
        # Use of gain K1 = -6.56683 balances ball for up to 8 minutes
        ## @brief Gain value for ball position
        self.k1 = -6.56683
        ## @brief Gain value for platform tilt
        self.k2 = -1.9586
        ## @brief Gain value for ball velocity
        self.k3 = -1.7126
        ## @brief Gain value for platform rotational velocity
        self.k4 = -0.20261
        
        # Motor Parameters
        ## @brief Terminal resistance from motor manual
        self.Resistance = 2.21 #ohms
        ## @brief Gain value from motor manual
        self.K_t = 0.0138 #N-m/A
        ## @brief Voltage value from motor manual
        self.V_dc = 12 #volts
        
    def run(self):
        """ @brief          Updates torque output for each motor based on positional
                            and velocity variables above                            
            @details        This function reads the ball position, platform tilt,
                            ball velocity, and platform rotational velocity values
                            from the panel and IMU, uses the corresponding gains
                            for each value and outputs a required torque 
                            which is converted to a duty cycle input for each motor.
        """
        
        T_x = (self.x.read() * self.k1 + 
               self.th.read() * self.k2 + 
               self.dx.read() * self.k3 + 
               self.dth.read() * self.k4)
        
        Duty = (100 * self.Resistance * T_x) / (4 * self.K_t * self.V_dc)
        if (Duty > 100):
            Duty = 100
        elif(Duty < -100):
            Duty = -100
        
        # print(self.touchbool.read())
        if (self.touchbool.read()):
            self.duty.write(Duty)
        else:
            self.duty.write(0)

            