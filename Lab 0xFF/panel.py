""" @file               panel.py
    @brief              Resistive touch panel driver 
    @details            This panel driver is responsible for 
                        interfacing with the resistive touch panel used for the 
                        term project. The driver allows for the calibration 
                        of the panel as well as the collection of positional 
                        data that is used along with a calculated velocity 
                        for the full state feedback controller.
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
"""

import pyb
import utime
import os
from micropython import const
from pyb import Pin

class Panel:
    def __init__(self):
        """ @brief              Initializes and returns a panel object.
            @details            Reads and writes calibration coefficients from text file to and from the designated
                                locations. Initializes settings of panel pins.
        """
        
        filename = "RT_cal_coeffs.txt"
        if filename in os.listdir():
            # File exists, read from it
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
        else:
            pass
            # File doesnt exist, calibrate manually and write the coefficients to the file
            with open(filename, 'w') as f:
                # Perform manual calibration
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = (0.048853735, 0.000343706, 0.000089394, 0.030606753, -100.5598926, -60.24475402)
                cal_values = [0.048853735, 0.000343706, 0.000089394, 0.030606753, -100.5598926, -60.24475402]
                # Then, write the calibration coefficients to the file
                # as a string. The example uses an f-string, but you can
                # use string.format() if you prefer
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
        
        ## @brief  Object setting output mode for the PYB
        self.PINOUT = const(pyb.Pin.OUT_PP)
        ## @brief  Object setting input mode for the PYB
        self.PININ = const(pyb.Pin.IN)
        ## @brief  Object establishing the negative y pin for the panel
        self.y_m = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        ## @brief  Object establishing the negative x pin for the panel
        self.x_m = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        ## @brief  Object establishing the positive y pin for the panel
        self.y_p = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        ## @brief  Object establishing the positive x pin for the panel
        self.x_p = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        
        # X Calibration
        ## @brief  Object setting 1 of 4 linear calibration constants
        self.K_xx = const(cal_values[0])
        ## @brief  Object setting 1 of 4 linear calibration constants
        self.K_xy = const(cal_values[1])
        ## @brief  Object setting 1 of 2 offset calibration values
        self.X_0 = const(cal_values[4])
        
        # Y Calibration
        ## @brief  Object setting 1 of 4 linear calibration constants
        self.K_yx = const(cal_values[2])
        ## @brief  Object setting 1 of 4 linear calibration constants
        self.K_yy = const(cal_values[3])
        ## @brief  Object setting 1 of 2 offset calibration values
        self.Y_0 = const(cal_values[5])
        
        ## @brief  Object defining the presence of an object on the touch panel
        self.touch = False
        ## @brief  Object for whether or not something is touching panel
        self.printboolean = False
        
        
    def collect_delay(self):
        """ @brief              Start time delay for data collection
            @details            Start time delay for data collection
        """
        self.start_ticks = utime.ticks_us()
            
            
    def get_coord(self):
        """ @brief              Collect data for the touch panel for ball position
            @details            Collect data for the touch panel for ball position
                                in the x, y, and z directions. For the z direction,
                                the panel senses whether or not an object is 
                                touching the panel surface.
            @return             tuple with x and y positions and touch status
        """
        # Start delay if something touches the panel
        if (self.touch == False):
            self.collect_delay()
        
        # Scan for X location
        self.x_m.low()
        self.x_p.high()
        self.y_m = Pin(Pin.cpu.A0, self.PININ)
        self.y_p = Pin(Pin.cpu.A6, self.PININ)
        self.x_ADC = pyb.ADC(self.y_m)
        self.x_voltage = self.x_ADC.read()
        self.y_p = Pin(Pin.cpu.A6, self.PINOUT)
        
        # Check if there is something on the panel
        self.y_p.high()
        self.x_p = Pin(Pin.cpu.A7, self.PININ)
        self.z_ADC = pyb.ADC(self.x_p)
        self.z_voltage = self.z_ADC.read()
        self.y_m = Pin(Pin.cpu.A0, self.PINOUT)
        
        # Scan for the Y location
        self.y_m.low()
        self.x_m = Pin(Pin.cpu.A1, self.PININ)
        self.y_ADC = pyb.ADC(self.x_m)
        self.y_voltage = self.y_ADC.read()
        self.x_m = Pin(Pin.cpu.A1, self.PINOUT)
        self.x_p = Pin(Pin.cpu.A7, self.PINOUT)
        
        self.x_pos = (self.x_voltage * self.K_xx) + (self.y_voltage * self.K_xy) + self.X_0
        self.y_pos = (self.x_voltage * self.K_yx) + (self.y_voltage * self.K_yy) + self.Y_0
        
        time_after_touch = utime.ticks_diff(utime.ticks_us(), self.start_ticks)
        
        if (self.z_voltage > 15):
            self.touch = True
        else:
            self.touch = False
        
        # Return data after a delay to account for the settling time response
        if (self.touch == True and time_after_touch > 5):
            self.printboolean = True
        else:
            self.printboolean = False
        
        return(tuple([self.x_pos, self.y_pos, self.touch]))
        
    def check_touch(self):
        """ @brief              Returns boolean for the z status of the touch panel
            @details            Returns boolean for the z status of the touch panel
            @return             Boolean for whether or not something is touching the panel
        """
        return self.printboolean
 