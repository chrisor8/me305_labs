""" @file               task_panel.py
    @brief              Script in charge of handling all the panel tasks.
    @details            This task handles all the tasks for the resistive touch panel. This task processes the data from
                        the resistive touch panel at the specified sample time. The task provides the option to use
                        either the raw data or the filtered data through the alpha-beta filtering method in lecture.
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
"""

import panel
import utime
from micropython import const

class Task_Panel:
    """ @brief                  Class responsible for handling all tasks regarding the resistive touch panel
        @details                This class has one main method that processes the data from the touch panel at
                                a specified time period. There is an option to either use the raw data or the filtered
                                data.
    """

    def __init__(self, x, y, dx, dy, touch, posfilter = False):
        """ @brief              Constructs a panel task
            @details            The panel task is in charge of outputting the linear position
                                and velocity of the ball in the X and Y direction. The position
                                and velocity of the ball are handled by a shares object which
                                will be read from the controller task.
            @param  x           A share. Share object describing the x position
                                of the ball.
            @param  y           A share. Share object describing the y position
                                of the ball.
            @param  dx          A share. Share object describing the x velocity
                                of the ball.
            @param  dy          A share. Share object describing the y velocity
                                of the ball.
            @param  touch       A share. Share object describing whether an object
                                is touching the panel.
            @param  posfilter   A boolean. Boolean describing whether to apply
                                an alpha beta filter to the data from the panel.
        """
        
        # Handle share objects
        ## @brief A shares object describing the position of the ball in the X direction
        self.x = x
        ## @brief A shares object describing the position of the ball in the Y direction
        self.y = y
        ## @brief A shares object describing the velocity of the ball in the X direction
        self.dx = dx
        ## @brief A shares object describing the velocity of the ball in the Y direction
        self.dy = dy
        
        ## @brief Object for the panel driver
        self.panel = panel.Panel()
        ## @brief Interval at which the task updates
        self.sampletime = 0.03
        ## @brief Boolean which determines whether to filter panel data
        self.posfilter = posfilter
        ## @brief A count for how many instances the finite state machine was ran
        self.runs = 0

        ## @brief Reference starting time
        self.start_ticks = utime.ticks_ms()
        ## @brief Next time at which to run the task
        self.next_ticks = utime.ticks_diff(utime.ticks_ms(), self.start_ticks) + self.sampletime*1_000
        
        # Filter Parameters
        ## @brief Alpha value for the alpha-beta filter
        self.alpha = const(0.85)
        ## @brief Beta value for the alpha-beta filter
        self.beta = const(0.005)
        ## @brief Boolean describing whether something is touching the panel
        self.touchbool = touch
        
        
    def run(self):
        """ @brief              The main function of the task panel
            @details            This function initializes the positional data needed to
                                apply the alpha beta filter. Depending on the
                                boolean status specified by the user, this function
                                will either apply a filter to the data or output
                                the raw data.
        """
        
        # Filter position data
        self.current_ticks = utime.ticks_ms()
        # Get the coordinates on the panel
        self.pos = self.panel.get_coord()

        # If this is the first instance of the FSM, establish the necessary values
        if self.runs == 0:
            self.x_pos = self.pos[0]
            self.x_ppos = self.x_pos
            self.x_pppos = self.x_pos
            self.x_vvel = 0
            self.x_vvvel = self.x_vvel
            
            self.y_pos = self.pos[1]
            self.y_ppos = self.y_pos
            self.y_pppos = self.y_pos
            self.y_vvel = 0
            self.y_vvvel = self.y_vvel

        # Process the data at the specified time period
        if (utime.ticks_diff(self.current_ticks, self.start_ticks) - self.next_ticks >= 0):
            if self.posfilter:
                # Filter on
                self.x_pppos = self.x_ppos + self.alpha*(self.pos[0] - self.x_ppos) + self.sampletime*(self.x_vvel)
                self.x_vvvel = self.x_vvel + (self.beta/self.sampletime)*(self.pos[0] - self.x_ppos)

                self.x.write(self.x_ppos/1_000)
                self.dx.write(self.x_vvel/1_000)
                self.x_ppos = self.x_pppos
                self.x_vvel = self.x_vvvel
                
                self.y_pppos = self.y_ppos + self.alpha*(self.pos[1] - self.y_ppos) + self.sampletime*(self.y_vvel)
                self.y_vvvel = self.y_vvel + (self.beta/self.sampletime)*(self.pos[1] - self.y_ppos)
                self.y.write(self.y_ppos/1_000)
                self.dy.write(self.y_vvel/1_000)
                self.y_ppos = self.y_pppos
                self.y_vvel = self.y_vvvel
            else:
                # Filter off
                self.x.write(self.pos[0]/1000)
                self.y.write(-1 * self.pos[1]/1000)
                self.x_vvel = (self.pos[0] - self.x_ppos) / self.sampletime
                self.y_vvel = ((-1 * self.pos[1]) - self.y_ppos) / self.sampletime
                self.dx.write(self.x_vvel/1000)
                self.dy.write(self.y_vvel/1000)
                self.x_ppos = self.pos[0]
                self.y_ppos = -1 * self.pos[1]

            # Determine next time to process the data
            self.next_ticks += self.sampletime * 1_000

        # Update the FSM count
        self.runs += 1
        # Write to shares object the boolean status of the touch panel
        self.touchbool.write(self.panel.check_touch())
