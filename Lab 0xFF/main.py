""" @file               main.py
    @brief              Main script for term project
    @details            Implements cooperative multitasking using tasks
                        implemented by finite state machines.
                        The finite state machine and task diagrams are shown below
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
    @image
"""

import shares
import task_user
import task_motor
import task_imu
import task_panel
import task_controller

def main():
    ''' @brief The main program
    '''

    ## @brief A shares.Share object describing the x position of the ball
    x_pos = shares.Share(0)
    ## @brief A shares.Share object describing the y position of the ball
    y_pos = shares.Share(0)
    ## @brief A shares.Share object describing the velocity of the ball in the x direction
    x_vel = shares.Share(0)
    ## @brief A shares.Share object describing the velocity of the ball in the y direction
    y_vel = shares.Share(0)
    ## @brief A shares.Share object describing the tilt of the platform about the x axis
    x_theta = shares.Share(0)
    ## @brief A shares.Share object describing the tilt of the platform about the y axis
    y_theta = shares.Share(0)
    ## @brief A shares.Share object describing the angular velocity of the platform about the x axis
    x_thetadot = shares.Share(0)
    ## @brief A shares.Share object describing the angular velocity of the platform about the y axis
    y_thetadot = shares.Share(0)
    ## @brief A shares.Share object handling the duty cycle for the motor about the x axis
    dutyX = shares.Share(0)
    ## @brief A shares.Share object handling the duty cycle for the motor about the y axis
    dutyY = shares.Share(0)
    ## @brief A shares.Share object describing whether or not something is touching the resistive touch panel
    touch = shares.Share(False)
    ## @brief A shares.Share object designating the state of the finite state machine
    state = shares.Share(1)
    ## @brief An IMU task object
    IMU = task_imu.Task_IMU(x_theta, y_theta, x_thetadot, y_thetadot)
    ## @brief A panel task object
    PANEL = task_panel.Task_Panel(x_pos, y_pos, x_vel, y_vel, touch, posfilter=False)
    ## @brief A controller task object for the motor rotating about the x axis
    CONTROLLER_X = task_controller.Task_Controller(x_pos, y_theta, x_vel, y_thetadot, dutyX, touch)
    ## @brief A controller task object for the motor rotating about the y axis
    CONTROLLER_Y = task_controller.Task_Controller(y_pos, x_theta, y_vel, x_thetadot, dutyY, touch)
    ## @brief A motor task object
    MOTOR = task_motor.Task_Motor(dutyX, dutyY, state)
    ## @brief A user task object
    USER = task_user.Task_User(state)

    while(True):
        try:
            PANEL.run()
            IMU.update()
            CONTROLLER_X.run()
            CONTROLLER_Y.run()
            MOTOR.run()
            USER.update()
                            
        except KeyboardInterrupt:
            break

    print('Program Terminating')
        
if __name__ == '__main__':
    main()
