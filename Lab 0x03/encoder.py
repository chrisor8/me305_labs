""" @file               encoder.py
    @brief              Encoder driver for motor 
    @details            This encoder driver is responsible for interfacing
                        with the encoder. This driver accounts for the counter
                        auto-reload to get the corrected position of the encoder.
    @author             Damond Li
    @author             Chris Or
    @date               November 2, 2021
"""

import pyb

class Encoder:
    """ @brief                  Interface with quadrature encoders
        @details                This class will be referenced in the encoder 
                                task.
    """
    
    def __init__(self, encoder_select):
        """ @brief              Constructs an encoder object
        """
        if (encoder_select == 2):
            ## @brief Object for PinB6
            self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
            ## @brief Object for PinB7
            self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
            ## @brief Establish the period and prescaler for the timer
            self.timer = pyb.Timer(4, prescaler = 0, period = 65535)
            ## @brief Object for timer channel 1
            self.t4ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin = self.pinB6)
            ## @brief Object for timer channel 2
            self.t4ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin = self.pinB7)
            ## @brief Reference posiiton 1
            self.pos_1 = self.timer.counter()
            ## @brief Reference position 2
            self.pos_2 = self.timer.counter()
            ## @brief Posiition of the motor
            self.position = self.timer.counter()
        elif (encoder_select == 1):
            ## @brief Object for PinC6
            self.pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
            ## @brief Object for PinC7
            self.pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
            ## @brief Establish the period and prescaler for the timer
            self.timer = pyb.Timer(8, prescaler = 0, period = 65535)
            ## @brief Object for timer channel 1
            self.t8ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin = self.pinC6)
            ## @brief Object for timer channel 2
            self.t8ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin = self.pinC7)
            ## @brief Reference posiiton 1
            self.pos_1 = self.timer.counter()
            ## @brief Reference position 2
            self.pos_2 = self.timer.counter()
            ## @brief Posiition of the motor
            self.position = self.timer.counter()
        
    def update(self):
        """ @brief              Updates encoder position and delta
            @details            Updates encoder position and delta by 
                                adjusting for the physical limitations of the 
                                encoder. This runs as fast as possible.
        """
        
        # Establish current and previous positions to determine delta
        self.pos_1 = self.pos_2
        self.pos_2 = self.timer.counter()
        delta_counter = self.pos_2 - self.pos_1
        
        # Account for the overflow or auto-reload
        if delta_counter > 55000:
            self.corrected_delta = delta_counter - 65535
        elif delta_counter < -55000:
            self.corrected_delta = delta_counter + 65535
        else:
            self.corrected_delta = delta_counter
            
        # Update the position accordingly
        self.position += self.corrected_delta
        
    def get_position(self):
        """ @brief              Returns encoder position
            @return             The position of the encoder shaft
        """
        return self.position
    
    def set_position(self, position):
        """ @brief              Sets encoder position
            @param  position    The new position of the encoder
        """
        self.position = position
        
    def get_delta(self):
        """ @brief              Retrieves the corrected change in position
            @return             The change in position of the encoder shaft
                                between the two most recent updates
        """
        return self.corrected_delta

            