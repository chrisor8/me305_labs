var searchData=
[
  ['get_0',['get',['../classshares_1_1Queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares::Queue']]],
  ['get_5faccel_1',['get_accel',['../classBNO055_1_1BNO055.html#a1676a6454e0615f8a84ff5411811daa0',1,'BNO055::BNO055']]],
  ['get_5fcalibration_5fcoeff_2',['get_calibration_coeff',['../classBNO055_1_1BNO055.html#af06124d01a721c06513c8e4af1d156d2',1,'BNO055::BNO055']]],
  ['get_5fcalibration_5fstatus_3',['get_calibration_status',['../classBNO055_1_1BNO055.html#aaa9dfe166380cf324009a27060cf00c7',1,'BNO055::BNO055']]],
  ['get_5fdelta_4',['get_delta',['../classencoder_1_1Encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder::Encoder']]],
  ['get_5feuler_5',['get_euler',['../classBNO055_1_1BNO055.html#a1873696ee7301f1188f5cdbbeb1d1900',1,'BNO055::BNO055']]],
  ['get_5fkp_6',['get_Kp',['../classclosedloop_1_1ClosedLoop.html#aabfd126eb373a40747f7fd312ed0056c',1,'closedloop::ClosedLoop']]],
  ['get_5fomega_7',['get_omega',['../classBNO055_1_1BNO055.html#a5cc089e83e452a99b718645047c07f43',1,'BNO055.BNO055.get_omega()'],['../classtask__encoder_1_1Task__Encoder.html#af183a396250e5ffa007ec298ac163c87',1,'task_encoder.Task_Encoder.get_omega()']]],
  ['get_5fposition_8',['get_position',['../classencoder_1_1Encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder::Encoder']]]
];
