var searchData=
[
  ['set_5fduty_0',['set_duty',['../classDRV8847_1_1Motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847::Motor']]],
  ['set_5fkp_1',['set_Kp',['../classclosedloop_1_1ClosedLoop.html#ab0e89bd08c6496dbc21ffa926e578b94',1,'closedloop::ClosedLoop']]],
  ['set_5fposition_2',['set_position',['../classencoder_1_1Encoder.html#a097746ac59abf28e6567f5604fe83c1f',1,'encoder::Encoder']]],
  ['set_5fspeed_3',['set_speed',['../classclosedloop_1_1ClosedLoop.html#aae404ca67bf70bf2a99e5c7fcfb5ae35',1,'closedloop::ClosedLoop']]],
  ['start_5fstep_4',['start_step',['../classtask__controller_1_1Task__Controller.html#a5dd3108563f38d58899934f8f06ea5e3',1,'task_controller.Task_Controller.start_step()'],['../classtask__motor_1_1Task__Motor.html#a47dffd3fbe2ae480a03adf12f5cc29cb',1,'task_motor.Task_Motor.start_step()']]],
  ['startdatacollect_5',['startDataCollect',['../classtask__encoder_1_1Task__Encoder.html#a6160065ffd127730e7ef57f5b8d286a1',1,'task_encoder::Task_Encoder']]],
  ['step_6',['step',['../classtask__motor_1_1Task__Motor.html#a79d55c2584eccce260651a3b8f4c883c',1,'task_motor::Task_Motor']]]
];
