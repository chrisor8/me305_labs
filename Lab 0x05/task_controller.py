""" @file               task_controller.py
    @brief              Encoder task for the encoder driver.
    @details            This task is responsible for establishing the frequency
                        at which data is collected from the encoder driver.
    @author             Damond Li
    @author             Chris Or
    @date               November 16, 2021
"""

import closedloop
import utime

class Task_Controller:
    """ @brief
        @details
    """

    def __init__(self, gain, speed):
        """ @brief              Constructs an encoder task
            @details            The encoder task is implemented as a finite
                                state machine.
            @param  state       A share. Share object describing the state of
                                the finite state machine.
        """
        ## @brief Create motor object
        self.controller = closedloop.ClosedLoop()
        self.gain = gain
        self.speed = speed
        self.period = 0.20
        self.boolean = False
        
    def run(self):
        """ @brief              Runs one iteration of the finite state machine.
            @details            This function calls to the encoder driver at
                                the specified period.
        """
        self.controller.update()
        if (self.gain.num_in() != 0):
            self.controller.set_Kp(self.gain.get())
        
        if (self.speed.num_in() != 0):
            self.controller.set_speed(self.speed.get())
        
        if (self.state.read() == 7):
            if (self.boolean == True):
                self.current_ticks = utime.ticks_ms()
                self.current_time = utime.ticks_diff(self.current_ticks, 
                                                     self.start_ticks) / 1_000
                if (utime.ticks_diff(self.current_ticks, self.start_ticks) < 10_000):
                    if (utime.ticks_diff(self.current_ticks, self.next_time) >= 0):
                        self.UI_print.put(tuple([utime.ticks_diff(
                            self.current_ticks, self.start_ticks) / 1_000, 
                            self.controller.omega, self.controller.output]))
                        self.next_time += self.period*1_000
                else:
                    self.end_step()
            
    def start_step(self):
        """ @brief              Starts data collection
            @details            This function initiates the collection of time 
                                and encoder position for 30s at intervals 
                                specified by the period in main. The
                                reference time is established.
        """
        self.boolean = True
        self.start_ticks = utime.ticks_ms()
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period*1_000)
        
    def end_step(self):
        """ @brief              Starts data collection
            @details            This function initiates the collection of time 
                                and encoder position for 30s at intervals 
                                specified by the period in main. The
                                reference time is established.
        """            
        if (self.boolean == True):
            self.boolean = False
            self.state.write(0)
        else:
            # Simply transition to state 0 if data collection did not start yet.
            self.state.write(0)
            