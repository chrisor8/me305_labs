# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 12:38:23 2021

Christopher Or
ME305-04L
"""

def fib (idx):
    '''
    @brief calculates a Fibonacci number at a specific index
    @param idx An integer specifying the index of the desired
                Fibonacci number
    '''
    
    # establish initial value f0 = 0
    value = 0
    # establish initial array for f0 and f1
    array = [0, 1]
    
    # give value f1 = 1
    if idx == 1:
        value = 1
        return value
    # fn where n is 2 or greater:
    # work from bottom up starting at 0 and 1 up to desired input value for n
    n = 0
    while n < idx - 1:
        value = array[n] + array[n + 1]
        array.append(value)
        n = n + 1
        
    return value
        
    # original equation fib [idx] = fib[idx-1] + fib[idx-2]


if __name__ == '__main__':

    # use try/except to ensure the input value is both a positive and int value
    try:
        # initial prompt to enter positive, int value
        idx = input('Please enter a positive, integer index value: ')
        # turns idx into an int value from string value
        idx = int(idx)
        
        # condition if idx is input as negative by user, returns error.
        if idx < 0:
            print ('Error: Please enter a value greater than or equal to zero.')
        # prints the resulting solution if both conditions are satisfied
        # that is, if input idx is positive and integer
        else:
            print('Fibonacci number at '
               'index {:} is {:}.'.format(idx,fib(idx)))
    # exception is for if non-integer which returns error
    except ValueError:
        print ('Error: Please enter a positive, integer value.')
        
  