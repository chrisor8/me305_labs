""" @file               main.py
    @brief              Main script for Lab 0x03
    @details            Implements cooperative multitasking using tasks
                        implemented by finite state machines.
                        The finite state machine and task diagrams (Lab0x02) are attached at the following link:
                        https://drive.google.com/file/d/1PL-Yptk5arjgVjXm8p4_OT9kfoPnJdpc/view?usp=sharing
    @author             Damond Li
    @author             Chris Or
    @date               November 2, 2021
"""

import shares
import task_encoder
import task_user
import task_motor

def main():
    ''' @brief The main program
    '''
    print('Welcome!' 
         ' \nz:         Zero encoder 1'
         ' \nZ:         Zero encoder 2'
         ' \np:         Print current position of encoder 1'
         ' \nP:         Print current position of encoder 2'
         ' \nd:         Print the delta for encoder 1'
         ' \nD:         Print the delta for encoder 2'
         ' \nm:         Set duty cycle of motor 1'
         ' \nM:         Set duty cycle of motor 2'
         ' \nc or C:    Clear motor fault'
         ' \ng:         Collect and print 30s of encoder 1 data'
         ' \nG:         Collect and print 30s of encoder 2 data'
         ' \ns or S:    Stop the data collection prematurely')
    
    ## @brief A shares.Share object describing the state of the finite state machine, starting at State 0
    STATE1 = shares.Share(0)
    ## @brief A shares.Share object describing the state of the finite state machine, starting at State 0
    STATE2 = shares.Share(0)
    ## @brief A shares.Queue object handling the duty cycle for motor 1
    DUTYCYCLE1 = shares.Share(0)
    ## @brief A shares.Queue object handling the duty cycle for motor 2
    DUTYCYCLE2 = shares.Share(0)
    ## @brief A shares.Queue object handling the return outputs from task 1, initially empty
    PRINT1 = shares.Queue()
    ## @brief A shares.Queue object handling the return outputs from task 1, initially empty
    PRINT2 = shares.Queue()
    
    GAIN1 = shares.Queue()
    
    SPEED1 = shares.Share(0)
    
    GAIN2 = shares.Queue()
    
    SPEED2 = shares.Share(0)
    
    DESIREDSPEED1 = shares.Share(0)
    DESIREDSPEED2 = shares.Share(0)
    
    ## @brief An encoder task object, encoder 1
    enctask1 = task_encoder.Task_Encoder(period=0.20, state=STATE1, UI_print=PRINT1, encodernum=1)
    ## @brief A user interface object, encoder 1
    usertask1 = task_user.Task_User(tasknum=1, state=STATE1, UI_print=PRINT1, duty_cycle = DUTYCYCLE1, gain=GAIN1, speed = SPEED1)
    ## @brief An encoder task object, encoder 2
    enctask2 = task_encoder.Task_Encoder(period=0.20, state=STATE2, UI_print=PRINT2, encodernum=2)
    ## @brief A user interface object, encoder 2
    usertask2 = task_user.Task_User(tasknum=2, state=STATE2, UI_print = PRINT2, duty_cycle = DUTYCYCLE2, gain=GAIN2, speed = SPEED2)
    ## @brief A motor task object responsible for handling both motors
    motortask = task_motor.Task_Motor(state1=STATE1, state2 = STATE2, duty_cycle1 = DUTYCYCLE1, duty_cycle2 = DUTYCYCLE2, speed1 = SPEED1, speed2 = SPEED2, desiredspeed1 = DESIREDSPEED1, desiredspeed2 = DESIREDSPEED2)
    motortask.motor_drv.enable()

    while(True):
        try:
            enctask1.run()
            usertask1.update()
            enctask2.run()
            usertask2.update()
            motortask.run()
            
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')

if __name__ == '__main__':
    main()
